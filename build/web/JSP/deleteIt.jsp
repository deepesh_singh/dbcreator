<%-- 
   jsp&html code for the delete operation in the table 
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Classes.ConnectDB"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Records</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <!CSS/Style of this page>
        <style>
            /*setting background picture */
            body{
                background-image: url('bg2.jpg');
                background-size: 100% 100%;

            }

            #top{

                background-color: rgba(10,10,77,0.6);
                height:100px;
            }
              /*customise add button*/
            .add{
                background: rgb(100,140,220);
                border: none;
                color: white;
                padding: 15px 32px;
                clear: left;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                margin-left:auto;
                margin-right:auto;
                width:60%;
            }
             /*customise submit button*/
            .submit{

                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 15px 32px;
                clear: left;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                margin-left:auto;
                margin-right:auto;
                width:60%;
            }
             /*customise form div*/
            #form{
                background-color:rgba(0,0,0,0.3);
                margin: auto auto;
                height:100%;
                width: 60%;
                padding: 30px 30px;

            }
            #heading{
                text-align: center;
                size:50px;
                color:white;
                font-family: Arial Rounded MT Bold;
                padding: 30px;

            }

            .text{

                color:wheat;
                font-size: 20px;
            }
             /*customise hover on submit button*/
            .submit:hover {

                background: #434343;
                letter-spacing: 2px;
                -webkit-box-shadow: 0px 5px 40px -10px rgba(20,36,50,0.57);
                -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
                box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
                transition: all 0.4s ease 0s;
            }
             /*customise hover on add button*/
            .add:hover {
                background: rgb(100,40,20);
                letter-spacing: 2px;
                -webkit-box-shadow: 0px 5px 40px -10px rgba(20,36,50,0.57);
                -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
                box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
                transition: all 0.4s ease 0s;
            }
            .name{
                border: none;
                border-bottom: 2px solid greenyellow;
                height:30px;
                width:250px;
                margin: 10px 10px;

            }

        </style>
    </head>
    <!body>
    <body>
        <!JSP code fo deleting any record from table>
        <%
            //checking if count parameter is null or not
            if (request.getParameter("count") != null) {
                delTable(response, request.getParameter("tname"), request.getParameter("clause"));
                out.print("<h3 align=center class=text>Record Deleted Successfully.</h3>");
            } else {
            }
        %>
        <%!
            //implement delTable method which is used to delete elements from the table with and without condition
            public void delTable(HttpServletResponse response, String name, String where) {

                boolean flag = false;
                String tablequery = "";
                PreparedStatement statement = null;
                //establishing connection with database using Connection object
                Connection con = ConnectDB.getConnection();

                if (where != null) {
                //query for deleting record depending on condition
                    tablequery = "DELETE from " + name + " where " + where;
                } else {
                     //query for deleting record without condition
                    tablequery = "DELETE from " + name;
                }
                try {
                    //use prepare to make datatype dynamic
                    statement = con.prepareStatement(tablequery);
                } catch (SQLException ex) {
                        System.out.print(ex.getMessage());
                }
                try {
                    statement.executeUpdate();
                    con.close();
                } catch (SQLException ex) {
                        System.out.print(ex.getMessage());
                }
            }
        %>
    
        <div id="form">
            <div id="top"><h1 id="heading">Welcome to database creator</h1></div>
            <!--making connection to home through submit type input-->
            <center> <input class="add" type="submit" onclick="location.href = 'index.jsp';" value="Home" ></center>
             
            
            <!--making the form-->
            <center>  <form action="" method="post">
                    <table>
                        <tr>
                        </tr>
                                <input type="hidden" name="count" value="1"/>
                                 <tr><td><label  class="text" >Select where clause</label> <input class="name" name="clause" ></td></tr>
                    </table>
                    <input class="submit" type="submit" value="Delete" >
                </form></center>
        </div>
    </body>
</html>
